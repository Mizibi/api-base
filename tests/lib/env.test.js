import { MandatoryEnvInt, MandatoryEnvString, MandatoryEnvBool, OptionalEnvInt, OptionalEnvString, OptionalEnvBool } from '../../src/lib/env/index'

const chai = require('chai')

describe('Env', function () {
    it('env int should be mandatory', () => {
        chai.expect(() => MandatoryEnvInt('PORT')).to.throw()
    })
    it('env int should be an integer mandatory', () => {
        process.env.PORT = 'string'
        chai.expect(() => MandatoryEnvInt('PORT')).to.throw()
    })
    it('env string should be mandatory', () => {
        chai.expect(() => MandatoryEnvString('LOCAL_URL')).to.throw()
    })
    it('env bool should be mandatory', () => {
        chai.expect(() => MandatoryEnvBool('PRODUCTION')).to.throw()
    })
    it('env int should be optional', () => {
        chai.expect(() => OptionalEnvInt('PRODUCTION')).not.to.throw()
    })
    it('env string should be optional', () => {
        chai.expect(() => OptionalEnvString('PRODUCTION')).not.to.throw()
    })
    it('env string should be optional', () => {
        chai.expect(() => OptionalEnvBool('PRODUCTION')).not.to.throw()
    })
})