import { Gender, UserRole } from '../types/enums';
import { User, UserRoleFields } from '../types/entities';
import { serializeEnum, deserializeEnum } from './serialize';

export function serializeUser<T extends Partial<User>>(user: T)
{
  return {
    ...user,
    gender: serializeEnum(Gender, user.gender),
  };
}

export function deserializeUser<T extends Partial<User>>(user: T)
{
  return {
    ...user,
    gender: deserializeEnum(Gender, user.gender),
  };
}

export const UserRoleToFieldMap = new Map<UserRole, keyof UserRoleFields>([
  [ UserRole.Admin, 'roleAdmin'],
]);

export const UserFieldsValidation = {
  login(value: string)
  {
    return value.length >= 1;
  },

  email(value: string)
  {
    //TODO: use an email checking lib
    return /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-z]+$/.test(value);
  },

  username(value: string)
  {
    return value.length >= 1;
  },

  password(value: string)
  {
    return value.length >= 1;
  },
};

export function hasUserRole(user: UserRoleFields, role: UserRole): boolean
{
  const field = UserRoleToFieldMap.get(role);
  return field && user[field] ? true : false;
}

export function transformUserRoleToField(role: UserRole, value: boolean): Partial<UserRoleFields>
{
  const res: Partial<UserRoleFields> = {};
  const field = UserRoleToFieldMap.get(role);
  if (field)
    res[field] = value;
  return res;
}
