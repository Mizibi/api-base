export function serializeEnum(enumType, value?: number | string)
{
  if (value === undefined)
    return undefined;
  if (typeof(value) === 'string')
    return value;
  return enumType[value];
}

export function deserializeEnum(enumType, value?: number | string)
{
  if (value === undefined)
    return undefined;
  if (typeof(value) === 'string')
    return enumType[value];
  return value;
}
