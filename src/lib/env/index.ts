import { InvalidEnvironment } from '../error';

class MissingEnvironmentVariable extends InvalidEnvironment {
    constructor(name, type) {
        super(name, `Missing environment variable: ${name} (expected ${type})`);
    }
}

class InvalidEnvironmentVariable extends InvalidEnvironment {
    constructor(name, type) {
        super(name, `Invalid environment variable: ${name} (expected ${type})`);
    }
}

export function MandatoryEnvInt(name: string): number {
    const value = process.env[name];
    if (value === undefined)
        throw new MissingEnvironmentVariable(name, 'int');
    const num = Number.parseInt(value);
    if (Number.isNaN(num))
        throw new InvalidEnvironmentVariable(name, 'int');
    return num;
}

export function MandatoryEnvString(name: string): string {
    const value = process.env[name];
    if (value === undefined)
        throw new MissingEnvironmentVariable(name, 'string');
    return value;
}

export function MandatoryEnvBool(name: string): boolean {
    const value: any = process.env[name];
    if (value === undefined)
        throw new MissingEnvironmentVariable(name, 'boolean');
    switch (value) {
        case true:
        case 'true':
        case 1:
        case '1':
            return true;
        case false:
        case 'false':
        case 0:
        case '0':
            return false;
    }
    throw new InvalidEnvironmentVariable(name, 'boolean');
}

function makeOptional<T>(f: (name: string) => T): (name: string) => T | null {
    return function (name: string) {
        if (process.env[name] === undefined)
            return null;
        return f(name);
    }
}

export const OptionalEnvInt = makeOptional(MandatoryEnvInt);
export const OptionalEnvString = makeOptional(MandatoryEnvString);
export const OptionalEnvBool = makeOptional(MandatoryEnvBool);
