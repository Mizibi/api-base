import { ServerError } from './base';

export class InvalidEnvironment extends ServerError {
    constructor(public key: string, message = 'Undefined environment variable') {
        super('InvalidEnvironment', message);
    }
}
