const ClientErrorIdentifier = Symbol();
const ServerErrorIdentifier = Symbol();

export function isClientError(error: Error): boolean {
    return error[ClientErrorIdentifier] === true;
}

export function isServerError(error: Error): boolean {
    return error[ServerErrorIdentifier] === true;
}

export abstract class ClientError extends Error {
    constructor(public code: string, public message: string) {
        super(message);
        this[ClientErrorIdentifier] = true;
    }
}

export abstract class ServerError extends Error {
    constructor(public code: string, public message: string) {
        super(message);
        this[ServerErrorIdentifier] = true;
    }
}

export class HiddenServerError extends ClientError {
    constructor() {
        super('ServerError', 'Internal Server Error');
    }
}
