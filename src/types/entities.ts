import { Gender } from './enums';

export interface TokenInfo
{
  token: string,
  expire: number/*Timestamp*/,
};

export interface Session extends TokenInfo
{
  id: number,
  userId: number | null,
};

export interface UserEditableFields
{
  email: string,
  username: string,
  firstname: string | null,
  lastname: string | null,
  gender: Gender,
  password: string,
};

export interface UserRoleFields
{
  roleAdmin: boolean,
};

export type User = {
  id: number,
  disabled: boolean,
  accountConfirmToken: string | null,
  passwordResetToken: string | null,
} & UserEditableFields & UserRoleFields;
