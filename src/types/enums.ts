export enum Gender
{
  Unspecified = 0,
  Male,
  Female,
};

export enum UserRole
{
  Admin,
};

export enum TokenType
{
  AccountConfirm,
  PasswordReset,
};
